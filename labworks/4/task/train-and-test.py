import argparse
import nltk

parser = argparse.ArgumentParser()
parser.add_argument('-f', '--corpus', action='store', default='corpus.txt', help='path to tagged corpus file')
parser.add_argument('-c', '--constraint', action='store', default=0, type=int, help='corpus size constraint')
parser.add_argument('-u', '--unsupervised', action='store_true', help='train unsupervised')
parser.add_argument('-i', '--iterations', action='store', default=5, type=int,
	help='max number of Baum-Welch interations to perform')

args = parser.parse_args()

with open(args.corpus) as file:
	tagged_sentences = [[nltk.tag.str2tuple(t) for t in line[:-1].split('\t')] for line in file]

print('Количество предложений в корпусе:', len(tagged_sentences))

if args.constraint > 0:
	tagged_sentences = tagged_sentences[:args.constraint]

size = int(len(tagged_sentences) * 0.9)
train_sentences = tagged_sentences[:size]
test_sentences = tagged_sentences[size:]

print('Количество предложений для обучения:', len(train_sentences))
print('Количество предложений для оценки:', len(test_sentences))
print()

if args.unsupervised:
	tags = set()
	symbols = set()
	unlabeled_sentences = []
	for sentence in train_sentences:
		unlabeled = []
		for token in sentence:
			tags.add(token[1])
			symbols.add(token[0])
			unlabeled.append((token[0], None))
		unlabeled_sentences.append(unlabeled)
	tags = list(tags)
	symbols = list(symbols)

	print('Обучение без учителя...')
	trainer = nltk.tag.hmm.HiddenMarkovModelTrainer(tags, symbols)
	tagger = trainer.train_unsupervised(unlabeled_sentences, max_iterations = args.iterations)	
else:
	print('Обучение с учителем...')
	trainer = nltk.tag.hmm.HiddenMarkovModelTrainer()
	tagger = trainer.train_supervised(train_sentences,
		estimator = lambda fd, bins: nltk.probability.LidstoneProbDist(fd, 0.1, bins))

print('Оценка:')
tagger.test(test_sentences)
sample = 'На улице светит солнце .'.split()
print(tagger.tag(sample))
print(tagger.best_path(sample))
print()
