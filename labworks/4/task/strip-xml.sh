#!/usr/bin/env bash

sed -E -n '
	/^<s>/,/^<\/s>/ {
		/^<s>/ {
			n
			s/([^\t]+)\t[^\t]+\t([^\t]+)\t.*/\1\/\2/
			h
			n
		}
		/^<\/s>/ !{
			s/([^\t]+)\t[^\t]+\t([^\t]+)\t.*/\1\/\2/
			H
		}
		/^<\/s>/ {
			g
			s/(<g\/>|@card@\/([^\n]+))\n?//g
			y/\n/\t/
			p
		}
	}
'
