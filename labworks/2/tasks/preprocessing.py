from nltk.corpus import stopwords
import string

punctuation = [*string.punctuation, "''", "``", "--"]

def process(words):
	words = [word.lower().strip("'\"_") for word in words if word not in punctuation]
	words = [word for word in words if word not in stopwords.words("english")]
	return words
