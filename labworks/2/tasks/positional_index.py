def create(tokenised_documents):
	word_map = {}
	for id, document in enumerate(tokenised_documents):
		for position, word in enumerate(document):
			postings = word_map.setdefault(word, {})
			postings.setdefault(id, []).append(position)
		
	return word_map

def search(index, token):
	result = set()
	if len(token) == 1 and token[0] in index:
		result = index[token[0]].keys()
	elif len(token) > 1:
		documents = index.get(token[0], {}).keys()

		for word in token[1:]:
			documents = documents & index.get(word, {}).keys()

		for document in documents:
			first_word_positions = index[token[0]][document]
			for position in first_word_positions:
				position += 1
				i = 1
				while i < len(token) and position in index[token[i]][document]:
					position += 1
					i += 1
				if i == len(token):
					result.add(document)
	return result