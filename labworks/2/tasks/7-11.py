import nltk
from nltk.util import bigrams
from nltk.tokenize import word_tokenize, sent_tokenize

import inverted_index
import positional_index
import preprocessing
import query

with open("pg8164.txt") as file:
	raw = file.read()

# Preprocess book

start = raw.find("MY MAN JEEVES\n")
end = raw.rfind("End of the Project Gutenberg EBook")
raw = raw[start:end]

# Split it on synthetic documents

sentences = sent_tokenize(raw)
n = 20
documents = [" ".join(sentences[i : i + n]) for i in range(0, len(sentences), n)]

# Tokenise and preprocess

tokenised = [preprocessing.process(word_tokenize(document)) for document in documents]

# Build inverted index for words and bigrams

index = inverted_index.create(tokenised)

bi_index = inverted_index.create([bigrams(document) for document in tokenised])

def print_postings(index, key):
	print(key, index.get(key))

print_postings(index, "jeeves")
print_postings(index, "cup")
print_postings(index, "tea")
print_postings(index, "o'clock")
print_postings(bi_index, ("jeeves", "man"))
print_postings(bi_index, ("morning", "tea"))
print_postings(bi_index, ("o'clock", "tea"))
print_postings(bi_index, ("cup", "tea"))

# Query inverted index

def search(token):
	return inverted_index.search(index, token[0])

q1 = query.create("cup AND tea", preprocessing.process)
print("cup AND tea", query.execute(q1, search))

print(documents[212])

q2 = query.create("tea OR cup", preprocessing.process)
print("tea OR cup", query.execute(q2, search))

# Build positional index

pos_index = positional_index.create(tokenised)

print_postings(pos_index, "tea")
print_postings(pos_index, "o'clock")

# Query positional index and inverted index for bigrams

def search_pos(token):
	return inverted_index.search(bi_index, token) if len(token) == 2 else positional_index.search(pos_index, token)

q3 = query.create("cup of tea OR teapot", preprocessing.process)
print("cup of tea OR teapot", query.execute(q3, search_pos))

print_postings(pos_index, "teapot")

q4 = query.create("tea, tea, tea AND teapot", preprocessing.process)
print("tea, tea, tea AND teapot", query.execute(q4, search_pos))

t5 = "I should imagine Jeeves feels very much the same about me."
q5 = query.create(t5, preprocessing.process)
print(t5, query.execute(q5, search_pos))

print(documents[4][1384:1484])

q5 = query.create("asd", preprocessing.process)
print("asd", query.execute(q5, search_pos))

q6 = query.create("cup of tea AND (morning OR doctor)", preprocessing.process)
print("cup of tea AND (morning OR doctor))", query.execute(q6, search_pos))

q7 = query.create("cup of tea AND (teapot OR (morning AND doctor))", preprocessing.process)
print("cup of tea AND (teapot OR (morning AND doctor))", query.execute(q7, search_pos))
