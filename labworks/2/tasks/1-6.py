import matplotlib
import nltk
from nltk.book import text1 as moby_dick
from nltk.corpus import gutenberg, stopwords
from nltk.tokenize import word_tokenize, sent_tokenize
import string

matplotlib.use('agg')

#1
with open("pg8164.txt") as file:
	raw = file.read()

#2
words = word_tokenize(raw)
sentences = sent_tokenize(raw)

#3
text = nltk.Text(words)

#4
frequency_distribution = nltk.FreqDist(text)
print(frequency_distribution.most_common(50))

frequency_distribution.plot(50)
matplotlib.pyplot.savefig("plot")

#5
frequency_distribution_md = nltk.FreqDist(moby_dick)
print(frequency_distribution_md.most_common(50))

#Both samples contain almost only stopwords and punctuation, but sample from book `My Man Jeeves' also contains the name of one of the main characters `Jeeves'.

#6
stoptokens = [*stopwords.words("english"), *string.punctuation, "''", "``", "--"]
frequency_distribution = nltk.FreqDist([token for token in text if token not in stoptokens])
print(frequency_distribution.most_common(50))
frequency_distribution_md = nltk.FreqDist([token for token in moby_dick if token not in stoptokens])
print(frequency_distribution_md.most_common(50))

#The most common word in both books is `I', because they both were written in the first person. Several words in samples are context relative. For example, in sample from `My Man Jeeves' there are names of characters: `George', `Jeeves'; and honorifics: `sir', `Mr'.
