def create(tokenised_documents):
	word_map = {}
	for i, document in enumerate(tokenised_documents):
		for word in document:
			entry = word_map.setdefault(word, set()).add(i)
	
	return word_map

def search(index, token):
	return index[token] if token in index else set()
