from nltk.tokenize import word_tokenize

def peek(stack):
	return stack[-1] if len(stack) > 0 else None

# Parse query string in infix notation and translate it
# to reverse Polish notation using shunting-yard algorithm.
def create(query_string, process):
	tokens = word_tokenize(query_string)
	stack = []
	result = []
	template = []
	for token in tokens:
		if token == "AND":
			result.append(tuple(process(template)))
			template = []
			while peek(stack) == "AND":
				result.append(stack.pop())
			stack.append(token)
		elif token == "OR":
			result.append(tuple(process(template)))
			template = []
			while peek(stack) in ["AND", "OR"]:
				result.append(stack.pop())
			stack.append(token)
		elif token == "(":
			stack.append(token)
		elif token == ")":
			if len(template) > 0:
				result.append(tuple(process(template)))
				template = []
			op = stack.pop()
			while op != "(":
				result.append(op)
				op = stack.pop()
		else:
			template.append(token)
	if len(template) > 0:
		result.append(tuple(process(template)))
	while len(stack) > 0:
		op = stack.pop()
		if op == "(":
			raise Exception()
		result.append(op)
	return result

# Do you know these songs?
#print(create("Hello again You've been alone awhile AND (Yesterday AND All you need is love OR sunny afternoon AND all day and all of the night)", lambda word: word))

#print(create("cup of tea AND (teapot OR (morning AND doctor))", lambda word: word))

def execute(query, search):
	stack = []
	for token in query:
		if isinstance(token, tuple):
			stack.append(search(token))
		else:
			operand_2 = stack.pop()
			operand_1 = stack.pop()
			result = (operand_1 & operand_2) if token == "AND" else (operand_1 | operand_2)
			stack.append(result)
	return stack.pop()
