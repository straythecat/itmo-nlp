import nltk

text = [token.lower() for token in nltk.word_tokenize('The cat saw a mouse in the kitchen')]

grammar = nltk.CFG.fromstring("""
S -> NP VP
VP -> V NP | V NP PP
PP -> P NP
NP -> DT N | DT N PP
DT -> 'a' | 'the'
N -> 'cat' | 'mouse' | 'kitchen'
V -> 'saw'
P -> 'in'
""")

parser = nltk.ChartParser(grammar)
trees = parser.parse_all(text)

for tree in trees:
	print(tree)