package main

import (
	"flag"
	"log"
	"os"
	"strings"

	"gitlab.com/straythecat/itmo-nlp/labworks/1/tasks/5/tabio"
)

func main() {
	flag.Parse()

	tabio.Write(os.Stdout, rows(process(tabio.Parse(os.Stdin))))
}

func process(tokens <-chan string) (cfreqs map[[3]string]uint, mfreqs [3]map[string]uint, n uint) {
	cfreqs = make(map[[3]string]uint)
	for i := 0; i < 3; i++ {
		mfreqs[i] = make(map[string]uint)
	}

	var i uint
	var lexemes [3]string
	for token := range tokens {
		if token == "\n" {
			if i != 3 {
				var row string
				if i < 3 {
					row = strings.Join(lexemes[:i+1], "\t")
				} else {
					row = strings.Join(lexemes[:], "\t") + "\t..."
				}
				log.Printf("invalid row: %s", row)
			} else {
				n++
				cfreqs[lexemes]++
			}
			i = 0
		} else if i > 2 {
			i++
		} else {
			lexemes[i] = token
			mfreqs[i][token]++
			i++
		}
	}
	
	return
}

func rows(cfreqs map[[3]string]uint, mfreqs [3]map[string]uint, n uint) <-chan []string {
	rows := make(chan []string)
	go func() {
		nstr := tabio.Utoa(n)
		var id uint
		for trigram, cfreq := range cfreqs {
			row := make([]string, 9)
			row[0] = tabio.Utoa(id)
			row[4] = tabio.Utoa(cfreq)
			for i := 0; i < 3; i++ {
				row[i+1] = trigram[i]
				row[i+5] = tabio.Utoa(mfreqs[i][trigram[i]])
			}
			row[8] = nstr
			rows <- row
			id++
		}
		close(rows)
	}()
	return rows
}
