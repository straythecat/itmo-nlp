package main

import (
	"flag"
	"os"
	"unicode"
	"unicode/utf8"

	"gitlab.com/straythecat/itmo-nlp/labworks/1/tasks/5/tabio"
)

func main() {
	flag.Parse()

	tabio.Write(os.Stdout, group(prefilter(tabio.Parse(os.Stdin))))
}

func prefilter(tokens <-chan string) <-chan string {
	filtered := make(chan string)
	go func() {
		for token := range tokens {
			r, _ := utf8.DecodeRuneInString(token)
			if !(len(token) == 1 && unicode.IsPunct(r)) {
				filtered <- token
			}
		}
		close(filtered)
	}()
	return filtered
}

func group(tokens <-chan string) <-chan []string {
	groups := make(chan []string)
	go func() {
		var first, second string
		var i uint = 0
		for token := range tokens {
			if token == "\n" {
				i = 0
			} else {
				switch i {
				case 0:
					i++
					first = token
				case 1:
					i++
					second = token
				case 2:
					groups <- []string{first, second, token}
					first, second = second, token
				}
			}
		}
		close(groups)
	}()
	return groups
}
