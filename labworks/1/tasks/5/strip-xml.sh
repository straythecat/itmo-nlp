#!/usr/bin/env bash

sed -E -n '
	/^<s>/,/^<\/s>/ {
		/^<s>/ {
			n
			s/[^\t]+\t([^\t]+)\t.*/\1/
			h
			n
		}
		/^<\/s>/ !{
			s/[^\t]+\t([^\t]+)\t.*/\1/
			H
		}
		/^<\/s>/ {
			g
			s/(<g\/>|@card@)\n?//g
			y/\n/\t/
			p
		}
	}
'
