package tabio

import (
	"bufio"
	"io"
	"log"
)

func Parse(in io.Reader) <-chan string {
	tokens := make(chan string)
	go func() {
		scanner := bufio.NewScanner(in)
		scanner.Split(split)
		for scanner.Scan() {
			tokens <- scanner.Text()
		}
		if err := scanner.Err(); err != nil {
			log.Fatalf("reading input: %s", err)
		}
		close(tokens)
	}()
	return tokens
}

func split(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if len(data) > 0 && data[0] == '\n' {
		return 1, data[:1], nil
	}
	for i := 0; i < len(data); i++ {
		if data[i] == '\t' {
			return i + 1, data[:i], nil
		}
		if data[i] == '\n' {
			return i, data[:i], nil
		}
	}
	if atEOF && len(data) > 0 {
		return len(data), data, nil
	}
	return
}
