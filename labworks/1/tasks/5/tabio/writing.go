package tabio

import (
	"bufio"
	"io"
	"log"
	"strconv"
)

func Write(out io.Writer, rows <-chan []string) {
	w := bufio.NewWriter(out)
	for row := range rows {
		last := len(row) - 1
		for i := 0; i < last; i++ {
			writeDelimitedString(w, row[i], '\t')
		}
		if last >= 0 {
			writeDelimitedString(w, row[last], '\n')
		}
	}
	if err := w.Flush(); err != nil {
		log.Fatalf("writing output: %s", err)
	}
}

func Utoa(u uint) string {
	return strconv.FormatUint(uint64(u), 10)
}

func writeDelimitedString(w *bufio.Writer, str string, delimiter rune) {
	if _, err := w.WriteString(str); err != nil {
		log.Fatalf("writing output: %s", err)
	}
	if _, err := w.WriteRune(delimiter); err != nil {
		log.Fatalf("writing output: %s", err)
	}
}
