package main

import (
	"flag"
	"log"
	"math"
	"os"
	"sort"
	"strconv"

	"gitlab.com/straythecat/itmo-nlp/labworks/1/tasks/5/tabio"
)

type AssociationMeasure func(cfreq uint, mfreqs [3]uint, n uint) float64
type Row struct {
	prefix string
	cfreq  uint
	mfreqs [3]uint
	n      uint
}
type Measured struct {
	prefix string
	score  float64
}

var measureName = flag.String("m", "f",
	"association measure: f - frequency, ts - t-score, mi - MI, sll - simple log-likelihood")
var count = flag.Uint("c", 10, "count of cooccurences to print")
var frequencyTreshold = flag.Uint("t", 0, "frequency treshold")

func main() {
	flag.Parse()

	var measure AssociationMeasure
	switch *measureName {
	case "f":
		measure = frequency
	case "ts":
		measure = tScore
	case "mi":
		measure = mi
	case "sll":
		measure = simpleLL
	default:
		log.Fatal("invalid measure name: %s", measureName)
	}

	tabio.Write(os.Stdout,
		selectTop(*count,
			calculate(measure,
				filter(*frequencyTreshold,
					rows(tabio.Parse(os.Stdin))))))
}

func rows(tokens <-chan string) <-chan *Row {
	rows := make(chan *Row)
	go func() {
		var i uint
		row := &Row{}
		for token := range tokens {
			switch i {
			case 0:
				row.prefix = token
				i++
			case 1, 2, 3:
				row.prefix += "\t" + token
				i++
			case 4:
				row.cfreq = atou(token)
				i++
			case 5, 6, 7:
				row.mfreqs[i-5] = atou(token)
				i++
			case 8:
				row.n = atou(token)
				rows <- row
				row = &Row{}
				i++
			case 9:
				i = 0
			default:
				log.Fatalf("invalid row: %s", row.prefix)
			}
		}
		close(rows)
	}()
	return rows
}

func atou(str string) uint {
	u, err := strconv.ParseUint(str, 10, 32)
	if err != nil {
		log.Fatalf("reading input: %s: %s", str, err)
	}
	return uint(u)
}

func filter(ft uint, rows <-chan *Row) <-chan *Row {
	filtered := make(chan *Row)
	go func() {
		for row := range rows {
			if row.cfreq >= ft {
				filtered <- row
			}
		}
		close(filtered)
	}()
	return filtered
}

func calculate(measure AssociationMeasure, rows <-chan *Row) <-chan *Measured {
	measured := make(chan *Measured)
	go func() {
		for row := range rows {
			score := measure(row.cfreq, row.mfreqs, row.n)
			measured <- &Measured{prefix: row.prefix, score: score}
		}
		close(measured)
	}()
	return measured
}

func selectTop(count uint, measured <-chan *Measured) <-chan []string {
	top := make([]*Measured, 0, count)

	for m := range measured {
		if i := search(top, m); i < count {
			l := uint(len(top))
			if l < count {
				top = top[:l+1]
			}
			if i < l {
				copy(top[i+1:], top[i:])
			}
			top[i] = m
		}
	}

	rows := make(chan []string)
	go func() {
		for _, t := range top {
			rows <- []string{t.prefix, strconv.FormatFloat(t.score, 'G', -1, 64)}
		}
		close(rows)
	}()
	return rows
}

func search(c []*Measured, s *Measured) uint {
	return uint(sort.Search(len(c), func(i int) bool { return c[i].score <= s.score }))
}

func tScore(cfreq uint, mfreqs [3]uint, n uint) float64 {
	return (float64(cfreq) -
		float64(mfreqs[0])*float64(mfreqs[1])*float64(mfreqs[2])/
			math.Pow(float64(n), 2)) /
		math.Sqrt(float64(cfreq))
}

func mi(cfreq uint, mfreqs [3]uint, n uint) float64 {
	return math.Log10(float64(cfreq) * math.Pow(float64(n), 2) /
		(float64(mfreqs[0]) * float64(mfreqs[1]) * float64(mfreqs[2])))
}

func simpleLL(cfreq uint, mfreqs [3]uint, n uint) float64 {
	var sll float64

	o := float64(cfreq)
	e := (float64(mfreqs[0]) * float64(mfreqs[1]) * float64(mfreqs[2])) / math.Pow(float64(n), 2)

	if o > 0 {
		sll = o * math.Log(o/e)
	}
	sll = 2.0 * (sll - (o - e))
	if o < e {
		sll = -sll
	}

	return sll
}

func frequency(cfreq uint, mfreqs [3]uint, n uint) float64 {
	return float64(cfreq)
}
