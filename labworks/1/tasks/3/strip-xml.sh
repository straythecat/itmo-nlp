#!/usr/bin/env bash

sed -E -n '
	/^<s>/,/^<\/s>/ {
		/^<s>/ {
			n
			s/\t.*//
			h
			n
		}
		/^<\/s>/ !{
			s/\t.*//
			H
		}
		/^<\/s>/ {
			g
			s/\n<g\/>\n//g
			y/\n/ /
			p
		}
	}
'
