from nltk.corpus import stopwords
import string

punctuation = [*string.punctuation, "''", "``", "--"]

def process(words):
	words = [word for word in words if (word not in punctuation and
		word.lower() not in stopwords.words("english"))]
	return words
