import argparse
import gensim.models
from nltk.tokenize import word_tokenize, sent_tokenize

import preprocessing

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--preprocess', action='store_true', help='enable preprocessing')
parser.add_argument('-w', '--workers', action='store', type=int, default=3, help='set number of worker threads')
parser.add_argument('file', action='store')

args = parser.parse_args()

with open(args.file) as file:
	raw = file.read()

sentences = sent_tokenize(raw)

if args.preprocess:
	tokenised = [preprocessing.process(word_tokenize(sentence)) for sentence in sentences]
else:
	tokenised = [word_tokenize(sentence) for sentence in sentences]

model = gensim.models.Word2Vec(tokenised, workers=args.workers, window=12, iter=25)

print(model)

model.wv.save_word2vec_format(args.file + '.model')
