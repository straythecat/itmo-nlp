import argparse
import sys

from pprint import pprint

import config

sys.path.append('nlp4is_word_embeddings/src')

from nlp4is_word_embeddings.src.w2v_dataset_helpers import print_latex_version

parser = argparse.ArgumentParser()
parser.add_argument('-e', '--evaluation', action='store', required=True, help="select evaluation: `analogies' or `doesnt-match'")

args = parser.parse_args()

if args.evaluation == 'analogies':
	import nlp4is_word_embeddings.src.analogies_evaluation as analogies

	for (method, emb_type) in config.METHODS:
		results = analogies.evaluate_analogies(method, emb_type)
		pprint(dict(results))
		print_latex_version(results, method, config.ANALOGIES_SECTIONS)

elif args.evaluation == 'doesnt-match':
	import nlp4is_word_embeddings.src.doesnt_match_evaluation as doesnt_match

	for (method, emb_type) in config.METHODS:
		results = doesnt_match.evaluate_doesnt_match(method, emb_type)
		pprint(dict(results))
		print_latex_version(results, method, config.DOESNT_MATCH_SECTIONS)
