from nlp4is_word_embeddings.datasets.create_questions import create_dataset
import os

path = os.getcwd() + "/datasets/"

create_dataset(sourcefile=path + "lotr_analogies.txt", outfile=path + "questions_lotr_analogies.txt", mode="analogies")
create_dataset(sourcefile=path + "lotr_doesnt_match.txt", outfile=path + "questions_lotr_doesnt_match.txt", mode="doesnt_match")
