import argparse
import os
import pickle
import random

parser = argparse.ArgumentParser()
parser.add_argument('-o', '--out', action='store', default='documents', help='path to directory for documents')
parser.add_argument('file', action='store')

args = parser.parse_args()

with open(args.file, 'rb') as file:
	properties = pickle.load(file)

with_aliases = [property for property in properties.items() if len(property[1]['aliases']) > 0]

sample = random.sample(with_aliases, 1000)

for property in sample:
	with open(os.path.join(args.out, property[0][1:]), 'w', encoding='utf8') as document:
		document.write('\n'.join([str(property[1]['label']), str(property[1]['description'])]))
