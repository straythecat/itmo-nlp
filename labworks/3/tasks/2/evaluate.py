import argparse
import os
import pickle

import search

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--dictionary', action='store', default='dictionary', help='path to dictionary file')
parser.add_argument('-p', '--postings', action='store', default='postings', help='path to postings file')
parser.add_argument('-i', '--documents', action='store', default='documents',
	help='path to directory with documents')
parser.add_argument('file', action='store', help='path to property file')

args = parser.parse_args()

with open(args.file, 'rb') as file:
	properties = pickle.load(file)

ids = [int(id) for id in os.listdir(args.documents)]

count = 0
found = 0
first = 0
position_sum = 0

for id in ids:
	queries = properties['P' + str(id)]['aliases']
	results = search.perform(args.dictionary, args.postings, queries)
	
	count += len(queries)
	for result in results:
		if id in result:
			found += 1
			position = result.index(id) + 1
			first += 1 if position == 1 else 0
			position_sum += position

print('Correct document was found in ', found / count, '% of cases (', found, ' out of ', count, ')')
print('The first document found was correct in ', first / found, '% of cases (', first, ' out of ', found, ')')
print('Average position of correct document was', position_sum / found)
