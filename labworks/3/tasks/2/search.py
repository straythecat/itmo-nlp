import ast
import sys
import types

with open('ranked-retrieval-search-engine/search.py') as file:
	root = ast.parse(file.read())

functions = {'load_dictionary', 'get_top_cosine_scores', 'get_query_terms', 'load_posting_list'}

for node in root.body[:]:
	if isinstance(node, ast.FunctionDef):
		if str(node.name) not in functions:
			root.body.remove(node)
	elif not isinstance(node, (ast.Import, ast.ImportFrom, ast.Assign)):
		root.body.remove(node)


module = types.ModuleType("rrse")
code = compile(root, 'rrse.py', 'exec')
sys.modules['rrse'] = module
exec(code, module.__dict__)

import rrse

def perform(dictionary_file, postings_file, queries):
	with open(dictionary_file) as file:
		loaded_dictionary = rrse.load_dictionary(file)
	
	dictionary = loaded_dictionary[0]
	vector_squares_sum = loaded_dictionary[1]
	
	results = []
	with open(postings_file, 'rb') as postings:
		for query in queries:
			result = rrse.get_top_cosine_scores(query, dictionary, postings, vector_squares_sum)
			results.append([r[0] for r in result])
	
	return results
